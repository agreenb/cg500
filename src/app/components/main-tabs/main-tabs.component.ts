import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'main-tabs',
  templateUrl: './main-tabs.component.html',
  styleUrls: ['./main-tabs.component.scss']
})
export class MainTabsComponent implements OnInit {

	   sections: any = [
   		 {
				name : "Music",
				key: "music",
				genres: ["All Music", "Rock", "Pop", "Country", "R&amp;B / Hip-Hop", "Christian", "Jazz/Standards", "Classical", "Dance/Electronic", "Latino", "Holiday", "Canadian" ]
			},{
				name : "Sports",
				key: "sports",
				genres: ["Sports", "NFL Play-by-Play", "MLB Play-by-Play", "College Play-by-Play", "NBA Play-by-Play", "NHL Play-by-Play", "Sports Play-by-Play"]
			},{
				name : "Talk",
				key: "talk",
				genres: ["All Talk", "Entertainment", "Comedy", "Religion", "Kids", "More"]
			},{
				name : "News",
				key: "news",
				genres: ["All News", "News/Public Radio", "Politics/Issues", "Traffic/Weather"]
			},{
				name : "Howard Stern",
				key: "howard",
				genres: ["Howard Stern"]
			},{
				name : "All",
				key: "all",
				genres: ["Featured"]
			}
   ];

  constructor() { }

  ngOnInit() {
  }

}
