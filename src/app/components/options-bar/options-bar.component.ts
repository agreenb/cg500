import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'options-bar',
  templateUrl: './options-bar.component.html',
  styleUrls: ['./options-bar.component.scss']
})
export class OptionsBarComponent implements OnInit {
	pkgselected = "allaccess";
	exportselected = "print";

  constructor() { }

  ngOnInit() {
  }

}
