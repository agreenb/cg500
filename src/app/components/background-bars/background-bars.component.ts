import { Component, OnInit } from '@angular/core';
import { CdkAccordionItem } from '@angular/cdk/accordion';
import { matExpansionAnimations, MatExpansionPanelState } from '@angular/material';

@Component({
  selector: 'background-bars',
  templateUrl: './background-bars.component.html',
  styleUrls: ['./background-bars.component.scss'],
  animations: [matExpansionAnimations.bodyExpansion],
})
export class BackgroundBarsComponent extends CdkAccordionItem implements OnInit {

   getExpandedState(): MatExpansionPanelState {
    return this.expanded ? 'expanded' : 'collapsed';
  }

  ngOnInit() { }

}