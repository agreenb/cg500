import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackgroundBarsComponent } from './background-bars.component';

describe('BackgroundBarsComponent', () => {
  let component: BackgroundBarsComponent;
  let fixture: ComponentFixture<BackgroundBarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackgroundBarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundBarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
