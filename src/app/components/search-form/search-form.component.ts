import {Component} from '@angular/core';
import {FormControl} from '@angular/forms';



export interface State {
  flag: string;
  name: string;
  population: string;
}

/**
 * @title Autocomplete overview
 */

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent {
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
}