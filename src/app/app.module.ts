import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppMaterialModule } from './app-material/app-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import 'hammerjs';


import { AppComponent } from './app.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { MainTabsComponent } from './components/main-tabs/main-tabs.component';
import { SubTabsComponent } from './components/sub-tabs/sub-tabs.component';
import { OptionsBarComponent } from './components/options-bar/options-bar.component';
import { BackgroundBarsComponent } from './components/background-bars/background-bars.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { ChannelListComponent } from './components/channel-list/channel-list.component';
import { TableHeaderComponent } from './components/channel-list/table-header/table-header.component';
import { TableCellsComponent } from './components/channel-list/table-cells/table-cells.component';
import { PageNotFoundComponent } from './components/not-found.component';



const sections: any = [
         {
        name : "All",
        key: "all",
        genres: ["Featured"]
      },{
        name : "Music",
        key: "music",
        genres: ["All Music", "Rock", "Pop", "Country", "R&amp;B / Hip-Hop", "Christian", "Jazz/Standards", "Classical", "Dance/Electronic", "Latino", "Holiday", "Canadian" ]
      },{
        name : "Sports",
        key: "sports",
        genres: ["Sports", "NFL Play-by-Play", "MLB Play-by-Play", "College Play-by-Play", "NBA Play-by-Play", "NHL Play-by-Play", "Sports Play-by-Play"]
      },{
        name : "News",
        key: "news",
        genres: ["All News", "News/Public Radio", "Politics/Issues", "Traffic/Weather"]
      },{
        name : "Howard Stern",
        key: "howard",
        genres: ["Howard Stern"]
      },{
        name : "Talk",
        key: "talk",
        genres: ["All Talk", "Entertainment", "Comedy", "Religion", "Kids", "More"]
      }
   ];



const appRoutes: Routes = [


  { path: 'music',      component: SubTabsComponent },
  { path: 'sports',      component: SubTabsComponent },
  { path: 'news',      component: SubTabsComponent },
  { path: 'howard',      component: SubTabsComponent },
  { path: 'talk',      component: SubTabsComponent },
  { path: 'all',      component: ChannelListComponent },

  { path: '',
    redirectTo: '/all',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    SearchFormComponent,
    MainTabsComponent,
    SubTabsComponent,
    OptionsBarComponent,
    BackgroundBarsComponent,
    NavBarComponent,
    ChannelListComponent,
    TableHeaderComponent,
    TableCellsComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FormsModule,
    OverlayModule,
     RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
