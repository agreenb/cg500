export class Query {
	constructor (
    public id?: string,
    public displayname?: string,
    public shortdescription?: string,
    public progtypetitle?: string,
    public genretitle?: string,
    public artistsyouhear?: string,
    ){}
}