import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    MatTableModule,
    MatSortModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule
  ],
  exports: [
  	MatTabsModule,
  	MatTableModule,
  	MatSortModule,
  	MatExpansionModule,
  	MatAutocompleteModule,
  	MatFormFieldModule,
  	MatIconModule,
  	MatButtonToggleModule,
  	MatSelectModule,
  	MatButtonModule,
  	MatCardModule,
  	MatInputModule

  ]
})
export class AppMaterialModule { }
